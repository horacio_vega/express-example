//controllers/core.js
/*Demo de utilizacion del module y controller
var customInterpolationApp = angular.module('test', []);

  customInterpolationApp.config(function($interpolateProvider) {
    $interpolateProvider.startSymbol('//');
    $interpolateProvider.endSymbol('//');
  });


  customInterpolationApp.controller('DemoController', function($scope, $http) {
	    $http({
	        method : "GET",
	        url : window.location.pathname+"/ajax/info"
	    }).then(function mySucces(response) {
	    	console.log(response.data.info);
	        $scope.response = response.data.info;//response.info;
	    }, function myError(response) {
	        $scope.response = response.statusText;
	    });
	});
  
*/


var dashboard = angular.module('Dashboard',[]);

dashboard.controller('mainController',['$scope' ,'$http' ,'serviceDashboard' , function($scope , $http , serviceDashboard){


	console.log(serviceDashboard);

	$scope.title = 'funciona';

	$scope.q = {}; //resultados del filtrado

	$scope.newItem = '';

	$scope.checkboxes = {
		valor1: true,
		valor2: true
	};

	$scope.radio = {
		show :true,
		disabled1:true,
		disabled2:true
	};
	
	$scope.lists = [
		{ id : 1,name :'Horacio vega tonomura',value : 'Horacio vega Castillo'},
		{ id : 2,name :'Hisashi Tonomura asahi',value : 'Tonomura Hisashi'},
		{ id : 3,name :'informacion nutrimental',value : 'nutrimental'},
	];


	//en este apartado ya estamos utilizando un servicio que obtiene los datos de la base de datos y los inserta.
	$scope.lists2;
	$scope.newItem2 = "";

	getItems();

	function getItems(){
		
		serviceDashboard.getItems()

		.success(function(items){
			//console.log(items);
			$scope.lists2 = items['data'];

			if(items['response'] == false)
				swal({title:"Advertencia.!",text:"No hay registros.",type:"error",timer:2000});

		})
		.error(function(error){
			swal({title:"Error.!",text:"Error al obtener los registros.",type:"error",timer:2000});
		});

	};

	$scope.addNew2 = function (){

//alert('no mms');
		//console.log($scope.newItem2);
		
		serviceDashboard.addNew($scope.newItem2)
		.success(function(response){
			if(response)
				getItems()
			else
				getItems()
		})
		.error(function(error){
			swal({title:"Error.!",text:"Error de red.",type:"error",timer:2000});
		});
		

	};


	$scope.addNew = function (){
	
		if($scope.newItem != ""){
			var object = {
			id : $scope.lists.lenght+1,
			name: $scope.newItem,
			value:$scope.newItem + " nuevo"
		};

		$scope.lists.push(object);	
		}else{
			swal({title:"No se puede agregar un registro vacio.!",text:"El campo registro es obligatorio.",type:"error",timer:2000});
		}
	};


	$scope.compara = function(e){
		//console.log($(this));
		//alert($scope.checkboxes.valor1);	
		if($scope.checkboxes.valor1 == "yes"){
			$scope.radio = {
				disabled1:false,
				disabled2:false
			};
		}if($scope.checkboxes.valor1 == "no"){
			$scope.radio = {
				show :true,
				disabled1:true,
				disabled2:true
			};
		}
	};

	var init = function(){

		$http({
		        method : "GET",
		        url : 'api/detail/pie-chart'
		    }).then(
		    	function mySucces(response){

			    	//console.log(response);
					//Se dibuja la gráfica de grafica Asistencia
					var graficaAsis = new google.visualization[response.data.type](document.getElementById('grafica1'));
					var data = google.visualization.arrayToDataTable((response.data.data));
					graficaAsis.draw(data,response.data.options);

		        },function myError(response) {
		        console.log('Error :' + response);
				swal({title:"No se encontro informacion de su busqueda.!",text:"No se encontro información para graficar.",type:"error",timer:2000});
		    });
	};

	setTimeout(function() {
    init();
	}, 2000);	

	
	
}]);



/*


*/