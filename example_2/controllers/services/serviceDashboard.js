dashboard.service('serviceDashboard', ['$http', function($http){

	this.addNew = function (newItem){

		var req = {
			method : 'POST',
			url : 'api/add-item',
			headers : {
				'Content-Type': 'application/json'
			},
			data : newItem
		};

		return $http(req);
		//return $http.post({'api/add-item',newItem,)};
	};

	this.getItems = function(){
		return $http.get('api/get-items');
	};

}]);