var DB = require('../db_conf');

var Disponibilidad = DB.Model.extend({
	tableName: 'sla_reporte_disponibilidad',
	idAttribute: 'id'
});

var AngularModel = DB.Model.extend({
	tableName : 'tbl_angular',
	idAttribute : 'id'
});

var add = function(values){
	console.log(values);
};

module.exports = {
	Disponibilidad : Disponibilidad, //Exporta el modelo ORM con bookshelf
	add : add, //solo exporta la funcion add
	DB : DB , //Es todo el objeto de la conexion a la BD al igual que knex
	AngularDB : AngularModel
};