var express = require('express');

var router = express.Router();

//Archivo de configuracion Manual
var config = require('./config');

// Database Connection
var Model = require('./models/model_disponibilidad');

var Items = [
		{id : 1 ,desc: 'test1'},
		{id : 2 ,desc: 'test2'},
		{id : 3 ,desc: 'test3'}
	];

router.get('/',function(req,res){

	res.render('dashboard/index',{
		conf : config.application,
		title: 'Dashboard',
		items : Items,

	});
	
});


router.post('/add',function(req,res){
	var newItem = req.body.newItem;
	Items.push({
		id : Items.lenght + 1,
		desc : newItem
	});
	res.redirect('/api');
});


//Ruta para guardar los registros en la Base de datos

router.post('/add-item',function(req,res){

	new Model.AngularDB(req.body).save().then(function(response){
		res.json(true);
	}).catch(function(error){
		res.json(false);
	});
	//console.log(res);
	//console.log(req.body.name);


	
	//res.redirect('/api');
});

router.get('/get-items',function(req,res){

	Model.DB.knex.select('*').from('tbl_angular').then(function(rows){

		info = {
			data : rows,
			response : true
		};
		res.json(info);

	}).catch(function(error){
		info = {
			response : false
		};
		res.json(info);
	});
	
	
});


router.get('/detail',function(req,res){
    res.render('detail/index',{
	conf : config,
	});
});

router.get('/detail/pie-chart',function(req,res){
	

	//var dataModel  =  Model.chartData('hoy');
	
	var raw_sel = "sum(case when reporta = 1 then 1 else 0 end) disponible, sum(case when reporta = 0 then 1 else 0 end) no_disponible,HOUR(fecha_captura) as tiempo,TIME_FORMAT(fecha_captura, '%H:00') as name_chart"; 

	Model.DB.knex.select(Model.DB.knex.raw(raw_sel))
	.from("sla_reporte_disponibilidad")
	.where('fecha_captura' ,'>=' , '2016-04-11 10:29:22' )
	.where('fecha_captura' ,'<=' , '2016-04-12 10:29:22' )
	.where('herramienta','=','1')
	.groupBy('tiempo')
	.then(function(rows){

		dataChart.push(["Task", "Hours per Day"]);

		console.log(process.memoryUsage());
		//model es mi objeto que tiene todos los valore de mi query
		rows.forEach(function(value){
		//Para cada objeto que contiene datos obtengo el campo de reporta
		var total = value['disponible'] + value['no_disponible'];
		var disponible = (value['disponible'] * 100) / 12;
		var no_disponible = (value['no_disponible'] * 100) / 12;

		dataChart.push([value['tiempo'], disponible]);
		console.log(value['tiempo']);

		});

		
		//dataChart.push(["Work", 11]);
		
	    var options = {
	    	title: 'My Daily Activities'
	    };

	    chartInfo = {
	    	data : dataChart,
	    	options : options,
	    	type : 'LineChart'
	    };
	   // console.log(res.json(chartInfo));
	    res.json(chartInfo);
		//console.log(rows);
	}).catch(function(error){
		console.log(error);
		//res.send('Ha ocurrido un error.');
	});;

	var dataChart = [];
	var chartInfo = []; 

	//console.log(dataModel);
	
	
		
});



module.exports = router;
