
var express = require('express');
var path = require('path');

/*
Este archivo contiene las configuracion del proyecto.

ejemplo

var Globals = {
    'domain':'www.info.com';
}
*/

var DB_Global_Config = {
host : '192.168.207.207',
user : 'root',
password : 's3gur0',
database : 'reporte_SLA',
charset : 'UTF8_GENERAL_CI'

};


var application = {
	 nameApp : 'GCP',
	 title : 'GCP',
	 urlImg : path.join(__dirname,'bower_components/custom/img')
};

module.exports = {
	application : application , 
	DB_Global_Config : DB_Global_Config
};
//module.exports = DB_Global_Config;

