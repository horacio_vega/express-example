var express = require('express');
var request = require('request');
var path = require('path');
var bodyParser = require('body-parser');
var logger = require('morgan');
//Definiendo Exprees Framework
var app = express();
// routes
//var route = require('./route');

app.set('view engine','ejs');
app.set('views',path.join(__dirname,'views'));

//Ocupo el body parser para obtener el scope de los elementos de 
//html de mi vista
app.use(bodyParser.urlencoded({'extended':'true'}));  
app.use(bodyParser.json({ /*type: 'application/*+json'*/ }));
app.use(logger('dev'));

//para cargar las librerias que se ocuparan.
app.use(express.static(path.join(__dirname,'bower_components')));
app.use(express.static(path.join(__dirname,'controllers')));


app.use('/api',require('./route'));

var port = process.env.PORT || 1337;

app.listen(port, function(){
	console.log('El servidor esta corriendo en el puerto 1337');
});