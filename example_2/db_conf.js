
//Archivo de configuracion Manual
var config = require('./config');

var configDB = {
host : config.DB_Global_Config.host,
user: config.DB_Global_Config.user,
password : config.DB_Global_Config.password,
database : config.DB_Global_Config.database,
charset : config.DB_Global_Config.charset
};

var dbConfig = ({
	client : 'mysql',
	connection : configDB,
});

var Promise = require('bluebird');
var knex = require('knex')(dbConfig);

var DB = require('bookshelf')(knex);


module.exports = DB;