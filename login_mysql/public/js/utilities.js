//$(document).on("click", closeDialog);
//$(document).on("keydown", closeDialogKey);

$.ajaxSetup({timeout:25000});
$(document).ajaxStart(function() {
	mostrarStatus();
});
$(document).ajaxStop(function() {
    ocultarStatus();
});

$(function() {
	$('.fecha').daterangepicker({
		singleDatePicker: true,
		showDropdowns: true,
		format: 'DD-MM-YYYY',
		minDate: fecha_minima,
		//maxDate: fecha_maxima,
		locale: {
			daysOfWeek: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi','Sa'],
			monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
			firstDay: 1
		}
	}, 
	function(start, end, label) {
	});
});


function mostrarStatus(){
    $("#shadow-black").show();
    $("#stbar").show();
}
function ocultarStatus(){
    $("#stbar").hide();
    $("#shadow-black").hide();
}
function validateForm(frm, formRules, formMessages){
	$(frm).validate({
		ignore: "",
		rules : formRules,
		messages: formMessages,
		errorClass: 'label-invalid',
	    invalidHandler: function(event, validator) {
		    var errors = validator.numberOfInvalids();
		    if (errors){
			    var message = errors == 1
			        ? 'Te falta 1 campo, se encuentra resaltado en la parte posterior'
				    : 'Te faltan ' + errors + ' campos, se encuentran resaltados en la parte posterior';
			    showAlert("error", message);
		    }
		 },
		highlight: function(element){
			$(element).addClass('mark-up-error');
		},
		unhighlight: function(element, errorClass, validClass){
			$(element).removeClass('mark-up-error');
		},
		errorPlacement: function(error, element){
			if (!flag){
				showAlert("error", error[0].innerHTML);
			}
		}
	});
}


function cleanForm(form) {
    $(form).find(':input').each(function () {
        switch (this.type) {
            case 'password':
            case 'select-multiple':
            case 'select-one':
            case 'text':
            case 'textarea':
                $(this).val('');
                break;
            case 'checkbox':
            case 'radio':
                this.checked = false;
        }
    });
}
function showAlert(status, message){
	alertBoxContainer = $('.alert-box-container');
	alertBox = $("#alert-box");
	alertBoxSpan = $("#alert-box p");
	cssClass = "";
	alertBox.removeClass();
	switch (status){
		case "success":
			alertBoxSpan.empty().html(message);
			alertBox.addClass('alert-box-success');
			cssClass = "alert-box-success";
			break;
		case "error":
			alertBoxSpan.empty().html(message);
			alertBox.addClass('alert-box-error');
			cssClass = "alert-box-error";
			break;
		case "info":
			break;
		case "warning":
			break;
	}
	alertBoxContainer.stop(true,true);
    alertBoxContainer
    .slideDown()
    .animate({opacity: 1.0}, 2000)
    .slideUp(function() { alertBox.removeClass(cssClass); });

}
function upperCase(event){
	input = $(event.currentTarget);
	input.val(input.val().toUpperCase());
}
function letra(e){var tecla=document.all?e.keyCode:e.which;if($.inArray(e.keyCode, [46,8,9,27,13,110,190]) !== -1 || (e.keyCode==65 && e.ctrlKey===true) || (e.keyCode>=35 && e.keyCode<=39)){return;}else{if(8==tecla)return false;var a=/[a-zA-ZáéíóúAÉÍÓÚÑñÜü ]/; var t=String.fromCharCode(tecla);return a.test(t)}}
function decimal(e){if($.inArray(e.keyCode,[46,8,9,27,13,110,190]) !== -1 || (e.keyCode==65 && e.ctrlKey===true) || (e.keyCode>=35 && e.keyCode<=39)){return true;}else{return tecla=document.all?e.keyCode:e.which,8==tecla?!0:(patron=/^([0-9])*[.]?[0-9]{0,2}$/,te=$("#"+this.id).val()+String.fromCharCode(tecla),patron.test(te)?void 0:!1)}}
function numero(e){if(e.which != 8 && e.which!=0 && (e.which<48||e.which>57)){return false;}}

$.ajaxSetup({
	timeout:25000,
	error: function(jqXHR, exception){
		if (jqXHR.status == 404) {
                showAlert("error","URL no encontrada.");
            } else if (jqXHR.status == 500) {
            	showAlert("error","Error interno del servidor.");
            } else if (exception === 'parsererror') {
                showAlert("error","Formato de respuesta inesperado.");
            } else if (exception === 'timeout') {
                showAlert("error","No se pudo procesar la peticion, intente nuevamente.");
            } else if (exception === 'abort') {
                showAlert("error","Se ha abortado la conexión.");
            }else{
            	showAlert("error","Error desconocido: " + jqXHR.responseText);
            }
        }
});
	function paginador(elemento,page,total,callback){
		if(page==1) first = "disabled"; else first = "";
		if(page==total) last = "disabled"; else last = "";
		html = '<button type="button" class="btn btn-sm first '+first+'" onclick="paginator_firstPage(\''+elemento+'\','+callback+');"><span class = "glyphicon glyphicon-step-backward"></span></a></button> ';
		html += '<button type="button" class="btn btn-sm prev '+first+'" onclick="paginator_prevPage(\''+elemento+'\','+callback+');"><span class = "glyphicon glyphicon-backward"></span></button> ';
		html += '<span class="pagedisplay"> '
		html += '<input type="text" id="'+elemento+'_page" name="'+elemento+'_page" readonly="readonly" style="width: 25px; text-align: center;" value="'+ page +'" /> ';
		html += 'de <input type="text" id="'+elemento+'_totalpage" name="'+elemento+'_totalpage" readonly="readonly" style="width: 25px; text-align: center;" value="'+total+'" /> ';
		html += '</span> <!-- this can be any element, including an input -->';
		html += '<button type="button" class="btn btn-sm next '+last+'" onclick="paginator_nextPage(\''+elemento+'\','+callback+');"><span class = "glyphicon glyphicon-forward"></span></button> ';
		html += '<button type="button" class="btn btn-sm last '+last+'" onclick="paginator_lastPage(\''+elemento+'\','+callback+');"><span class = "glyphicon glyphicon-step-forward"></span></button>';
		$('#'+elemento).html(html);
	}
	function paginator_firstPage(elemento,callback){
		page = $('#'+elemento+'_page');
		if(page.val()!=1){
			page.val(1);
			callback(1);
		}
	}
	function paginator_lastPage(elemento,callback){
		page = $('#'+elemento+'_page');
		total = $('#'+elemento+'_totalpage');
		if(page.val()!=total.val()){
			page.val(total.val());
			callback(total.val());
		}
	}
	function paginator_nextPage(elemento,callback){
		page = $('#'+elemento+'_page');
		total = $('#'+elemento+'_totalpage');
		if(page.val()<total.val()){
			page.val(parseInt(page.val())+1);
			callback(page.val());	
		}
	}
	function paginator_prevPage(elemento,callback){
		page = $('#'+elemento+'_page');
		if(page.val()>1){
			page.val(parseInt(page.val())-1);
			callback(page.val());	
		}				
	}
