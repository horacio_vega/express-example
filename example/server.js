// server.js

    // Dependencias ========================
    var express  = require('express');                   
    var mongoose = require('mongoose');                     // mongoose for mongodb
    var morgan = require('morgan');             // log requests to the console (express4)
    var bodyParser = require('body-parser');    // pull information from HTML POST (express4)
    var methodOverride = require('method-override'); // simulate DELETE and PUT (express4)
    var tools = require('./tools/db_connection'); // las funciones de la BD
    //MongoDB
    //mongoose.connect('mongodb://localhost/node_test');


	//	Express =============================
	var app      = express();  // create our app w/ express
    
    app.use(morgan('dev'));                                         // log every request to the console
    app.use(bodyParser.urlencoded({'extended':'true'}));            // parse application/x-www-form-urlencoded
    app.use(bodyParser.json());                                     // parse application/json
    app.use(bodyParser.json({ type: 'application/vnd.api+json' })); // parse application/vnd.api+json as json
    app.use(methodOverride());

    //Routes ====================================
    app.use('/api',require('./routes/api'));                 // set the static files location /public/img will be /img for users

    // listen (start app with node server.js) ======================================
    app.listen(8080);
    console.log("App listening on port 8080");
