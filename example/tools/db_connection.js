//Dependencies
var mysql = require('mysql');

//Mysql DB
var pool = mysql.createPool({
    connectionLimit : 100,
    host :'localhost',
    user : 'root',
    password : 'root',
    database : 'tmd_ventas_2',
    debug :true,

});


module.exports = {
  handle_database: function (req,res ,query) {
	  pool.getConnection(function(err,connection){
	        if (err) {
	          connection.release();
	          res.json({"code" : 100, "status" : "Error in connection database"});
	          return;
	        }  

	        console.log('connected as id ' + connection.threadId);
	       
	        connection.query(query,function(err,rows){
	            connection.release();
	            if(!err) {
	                res.json(rows);
	            }          
	        });

	        connection.on('error', function(err) {      
	              res.json({"code" : 100, "status" : "Error in connection database"});
	              return;    
	        });
	  });
  },
  bar: function () {
    // whatever
  }
};




